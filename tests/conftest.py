import pytest
from lib.Pet import Pet
from lib.utils import post_request, delete_request
from lib.defaults import APITOKEN


@pytest.fixture(autouse=True)
def set_APITOKEN():
    APITOKEN = 'test-api-octavian-balas'

@pytest.fixture()
def url():
    return 'http://petstore.swagger.io/v2/pet'

@pytest.fixture()
def use_pets():
    pet1 = Pet('Scooby', 'dogs', 'http://url.com/scooby.jpg', 'brown', 'available')
    pet2 = Pet('Lessie', 'dogs', 'http://url.com/lessie.jpg', 'gold', 'pending')
    pet3 = Pet('Garfield', 'cats', ['http://url.com/cat.jpg', 'http://url.com/garf.jpg'], 'gold', 'available')
    pet4 = Pet('Kitti', 'cats', 'http://url.com/kitti.jpg', 'gold', 'pending')
    return [pet1, pet2, pet3, pet4]

@pytest.fixture()
def ingest_pets(url, use_pets):
    ids = []
    for resource in range(0, 4):
        r = post_request(url, use_pets[resource].get_pet())
        ids.append(r['id'])
    yield 0
    for i in ids:
        delete_request(url, i)
