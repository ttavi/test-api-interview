import pytest
from lib.utils import *


@pytest.mark.usefixtures("url", "use_pets")
class TestPet:

    def test_post_get_by_id(self, url, use_pets):
        r = post_request(url, use_pets[0].get_pet())
        check = get_request("{}/{}".format(url, r['id']))
        assert use_pets[0].name == check['name']
        assert use_pets[0].status == check['status']
        assert use_pets[0].category == check['category']['name']
        assert use_pets[0].tags in [tag["name"] for tag in check['tags'] ]
        assert use_pets[0].photoUrls[0] in check['photoUrls']

    def test_post_negative(self, url):
        dummy_pet = {"tags": "abc"}
        post_request(url, dummy_pet, is_negative=True)

    def test_put(self, url, use_pets):
        # create a resource
        post_request(url, use_pets[0].get_pet())
        # update the resource locally
        new_tag = "new-tag"
        use_pets[0].update_tags(new_tag)

        # update the resource on the server
        r2 = put_request(url, use_pets[0].get_pet())

        # check the resource is updated on the server
        check2 = get_request("{}/{}".format(url, r2['id']))
        assert use_pets[0].tags in [tag["name"] for tag in check2['tags']]

    def test_put_negative(self, url):
        dummy_pet = {"tags": "abc"}
        put_request(url, dummy_pet, is_negative=True)

    def test_delete(self, url, use_pets):
        r1 = post_request(url, use_pets[0].get_pet())
        delete_request(url, r1['id'])

    def test_delete_negative(self, url):
        delete_request(url, -1234567, is_negative=True)

    @pytest.mark.parametrize("status", ["available", "pending", "sold"])
    def test_find_by_status(self, url, status, ingest_pets):

        check = find_by_status(url, status)
        for pet in check:
            assert pet['status'] == status







