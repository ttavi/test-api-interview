from random import randint


class Pet:

    def __init__(self, name, category, photoUrls, tags, status):
        self.id = randint(1, 100000)
        self.name = name
        self.category = category  # dict
        self.photoUrls = photoUrls if type(photoUrls) == list else [photoUrls] #list of url
        self.tags = tags #string
        self.status = status

        self._create_config()

    def _create_config(self, myid=-1):
        self.config = {
            "id": self.id,
            "name": self.name,
            "category": {
                "name": self.category
             },
            "photoUrls": self.photoUrls,
            "tags": [
                    {
                    "name": self.tags
                    }
                     ],
            "status": self.status
            }
        if myid != -1:
            self.config['id'] = myid

    def update_tags(self, new_tag):
        self.tags = new_tag
        self._create_config()

    def update_id(self, new_id):
        self._create_config(new_id)

    def get_pet(self):
        return self.config

PETEXAMPLE = {
    "id": 0,
    "category": {
        "id": 0,
        "name": "string"
    },
    "name": "doggie",
    "photoUrls": [
        "string"
    ],
    "tags": [
        {
            "id": 0,
            "name": "string"
        }
    ],
    "status": "available"
}