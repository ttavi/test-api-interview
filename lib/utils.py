import requests
import logging
import json
from defaults import HEADERS


def return_body(r, is_negative=False):
    logging.debug("Received : \n  RESPONSE CODE: {} {} \n CONTENT: {} ".format(r.status_code, r.reason, r.content))
    if is_negative:
        assert r.status_code != 200, "Got response {}. Is this a negative test?".format(r.status_code)
    else:
        assert r.status_code == 200, "Got response {}".format(r.status_code)
    try:
        return r.json()
    except ValueError: #for delete requests or negative causes
        return ""


def get_request(url):
    r = requests.get(url, headers=HEADERS)
    logging.debug("Sent : \n URL: {} \n HEADERS: {} ".format(url, r.headers))
    return return_body(r)


def post_request(url, data, is_negative=False):
    r = requests.post(url, data=json.dumps(data), headers=HEADERS)
    logging.debug("Sent : \n URL: {} \n HEADERS: {} \n DATA {} ".format(url, r.headers, json.dumps(data)))
    return return_body(r, is_negative)


def put_request(url, data, is_negative=False):
    r = requests.put(url, data=json.dumps(data), headers=HEADERS)
    logging.debug("Sent : \n URL: {} \n HEADERS: {} \n DATA {} ".format(url, r.headers, json.dumps(data)))
    return return_body(r, is_negative)


def delete_request(url, id, is_negative=False):
    r = requests.delete("{}/{}".format(url, id))
    logging.debug("Sent : \n URL: {} \n HEADERS: {}  ".format(url, r.headers))
    return return_body(r, is_negative)


def find_by_status(url, status):
    return get_request("{}/findByStatus?status={}".format(url, status))